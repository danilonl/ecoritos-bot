const bot = require('bbot')

/** This is a workaround for Heroku to confirm web binding. */
// @todo Remove this when bBot includes Express (next release)
const http = require('http')
const handle = (req, res) => res.end('hit')
const server = http.createServer(handle)
server.listen(process.env.PORT || 5000)

bot.global.text({
  before: ['+','-']																		// The message must end with '+' or '-'
}, async (b) => {
	var msg = b.message+''																//Get the full message
	var greedy = ''																		//Declare the 'greedy' variable, replacing the Buzz Kill


	var numberOfeCoritos = msg.split(" ")[msg.split(" ").length-1]						//Get the last element of the message
	var ecoritosTarget = msg.split(" ")[msg.split(" ").length-2]						//who is the lucky person or 'TAG' ? 

	if (numberOfeCoritos.length > 5){													//If more than 5, ignore all else and replace the empty 'greedy' 
		numberOfeCoritos = numberOfeCoritos.substring(0,5)
		greedy = ' (Maximum change of 5 eCoritos, don\'t be greedy!)'
	}
	var targetUser = bot.userById(ecoritosTarget)										//convert to User Object
	var giverUser = '@'+b.user.name														//Who is the Giver? this is to prevent self given eCoritos

	if (!targetUser.ecoritosvalue) {													//if the current Target does not have the property, create it!
		targetUser.ecoritosvalue = 0
	}

	if (giverUser != targetUser.name) {													//Preventing self given eCoritos
		switch ( numberOfeCoritos ) {													//Changing the value and sending the 'reply' so the Bot mention the giver
			case '+':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue + 1;
				return b.reply('gave eCoritos to '+ `${targetUser.name}` + ' . Got :military_medal: . Now have ' + `${targetUser.ecoritosvalue}`)
				break;
			case '++':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue + 2;
				return b.reply('gave eCoritos to '+ `${targetUser.name}` + ' . Got :military_medal::military_medal: . Now have ' + `${targetUser.ecoritosvalue}`)
				break;
			case '+++':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue + 3;
				return b.reply('gave eCoritos to '+ `${targetUser.name}` + ' . Got :military_medal::military_medal::military_medal: . Now have ' + `${targetUser.ecoritosvalue}`)
				break;
			case '++++':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue + 4;
				return b.reply('gave eCoritos to '+ `${targetUser.name}` + ' . Got :military_medal::military_medal::military_medal::military_medal: . Now have ' + `${targetUser.ecoritosvalue}`)
				break;
			case '+++++':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue + 5;
				return b.reply('gave eCoritos to '+ `${targetUser.name}` + ' . Got :military_medal::military_medal::military_medal::military_medal::military_medal: . Now have ' + `${targetUser.ecoritosvalue}` + `${greedy}`)
				break;
			case '-':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue - 1;
				return b.reply('took eCoritos from '+ `${targetUser.name}` + ' . Got :skull: . Now have ' + `${targetUser.ecoritosvalue}` )
				break;
			case '--':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue - 2;
				return b.reply('took eCoritos from '+ `${targetUser.name}` + ' . Got :skull::skull: . Now have ' + `${targetUser.ecoritosvalue}` )
				break;
			case '---':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue - 3;
				return b.reply('took eCoritos from '+ `${targetUser.name}` + ' . Got :skull::skull::skull: . Now have ' + `${targetUser.ecoritosvalue}` )
				break;
			case '----':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue - 4;
				return b.reply('took eCoritos from '+ `${targetUser.name}` + ' . Got :skull::skull::skull::skull: . Now have ' + `${targetUser.ecoritosvalue}` )
				break;
			case '-----':
				targetUser.ecoritosvalue = targetUser.ecoritosvalue - 5;
				return b.reply('took eCoritos from '+ `${targetUser.name}` + ' . Got :skull::skull::skull::skull::skull: . Now have ' + `${targetUser.ecoritosvalue}` + `${greedy}`)
				break;
		}
	}else{return b.reply('No eCoritos to yourself, that\'s the law!')}
}, {
  id: 'ping-ecoritos'
})


bot.global.direct(/My|MY|my/i, async (b) => {
  var ecoritosTarget ='@'+b.user.name
  var targetUser = bot.userById(ecoritosTarget)
  return b.reply('You Have ' + `${targetUser.ecoritosvalue}` + ' eCoritos')
}, {
  id: 'ping-my'
})

bot.start() // 🚀
